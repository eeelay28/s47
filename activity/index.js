// alert("Hello World");



// console.log(document.querySelector("#txt-first-name"));
// console.log(document);


// DOM MANIPULATION

const  txtFirstName = document.querySelector("#txt-first-name");

const  txtLastName = document.querySelector("#txt-last-name");

const  spanFullName = document.querySelector("#span-full-name");


console.log(txtFirstName);
console.log(spanFullName);


/* 
  Event:

    click
    hover
    keypress
    keyup

  Event Listener:

    allows us to let our users interact with our page. each click or hover is an event which can trigger a function or a task
  
  Syntax:
    selectedElement.addEventLister('event',function);
*/


txtFirstName.addEventListener('keyup',(event) => {
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
});

txtLastName.addEventListener('keyup',(event) => {
  spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
});

const labelFirstName = document.querySelector("#label-txt-name")

labelFirstName.addEventListener('click',(e) => {
  console.log(e)
  alert("You clicked the First Name label.")
})



txtFirstName.addEventListener('keyup',(event) => {
    console.log(event);
    console.log(event.target);
    console.log(event.target.value);
});

txtLastName.addEventListener('keyup',(event) => {
  console.log(event);
  console.log(event.target);
  console.log(event.target.value);
});
/* 
  innerHTML - property of an element which considers all the children of the selected element as a string. 

    .value of the input text field
*/